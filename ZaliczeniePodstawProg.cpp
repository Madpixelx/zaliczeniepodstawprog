﻿/// Specyfikacja programu
//1. program losujący 100 liczb, zapisujący je do tablicy i sortujący;
//2. zapisanie danych do pliku tekstowego;
//3. wyświetlenie tego pliku tekstowego;
//4.  odczytanie tych liczb na konsoli, a dokładniej wycinka tablicy korzystając ze znaczników(?).
///

#include "includes.h"

using std::cout;
using std::string;

// Definicje typów
typedef unsigned int UINT;
typedef std::chrono::high_resolution_clock Clock;

/// Prototypy funkcji
// Tabele
void TableFillRandom(long* arr[], unsigned int arrLen);
template <class T> inline T GetRandNum(int maxNum = 1000000);
inline void SaveFile(long* arr[], UINT arrLen);
inline string ReadFile();

// Sortowania
void SelectionSort(long* arr[], UINT n);
template <class T> inline void Swap(T& src, T& dest);

int main()
{
    UINT numberCount = 100;

    cout << "Losuje "<< numberCount << " liczb! \n";
    // Przydział pamięci
    long * arr = new long[numberCount];
    // Uzupełnienie tablicy losowymi liczbami
    TableFillRandom(&arr, numberCount);
    // Sortowanie
    SelectionSort(&arr, numberCount);
    cout << "Zapisuje wylosowane dane do pliku o nazwie dane.txt w katalogu programu! \n";
    SaveFile(&arr,numberCount);
    cout << "Odczyt zapisanych danych w pliku dane.txt: \n"
         << "========================================== \n";
    cout << ReadFile() << "\n"
        << "========================================== \n"
        << "Koniec wyjscia programu, nacisnij enter aby zakonczyc prace \n";
    std::cin.get();

}
inline void SaveFile(long* arr[], UINT arrLen) {
    std::ofstream file("dane.txt");
    if (file.is_open()) {
        for (UINT i = 0; i < arrLen; i++) {
            file << (*arr)[i] << "\n";
        }
        file.close();
    }
    else {
        cout << "Nie mozna otworzyc pliku!\n";
    }
}
inline string ReadFile() {
    std::ifstream file("dane.txt");
    std::string fileContent;
    if (file.is_open()) {
        fileContent.assign((std::istreambuf_iterator<char>(file)),
            (std::istreambuf_iterator<char>()));
    }
    return fileContent;
}

void TableFillRandom(long *arr[], UINT arrLen) {
    for (UINT i = 0; i < arrLen; i++) {
        (*arr)[i] = GetRandNum<long>();;
    }
}

void SelectionSort(long* arr[], UINT arrLen)
{
    UINT i, j, min_index;

    for (i = 0; i < arrLen - 1; i++)
    {
        min_index = i;
        for (j = i + 1; j < arrLen; j++)
            if ((*arr)[j] < (*arr)[min_index])
                min_index = j;

        Swap((*arr)[min_index], (*arr)[i]);
    }
}

// Swap
template <class T>
inline void Swap(T& src, T& dest)
{
    int temp = src;
    src = dest;
    dest = temp;
}


template <class T>
inline T GetRandNum(int maxNum) {
    std::mt19937 mTw; // Mersenne Twister
    auto seedTime = Clock::now().time_since_epoch().count();
    mTw.seed(seedTime);
    std::uniform_int_distribution<int> distro(1, maxNum);
    return distro(mTw);
}

